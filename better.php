<?php

class better {
    public static function concat(array $input): string
    {
        return array_reduce($input,
            static function (?string $carry, string $item) {
                return $carry . $item . ', ';
            }
        );
    }
}

