<?php declare(strict_types=1);

namespace Hyperized\HelloWorld\Tests;

use Hyperized\HelloWorld;
use PHPUnit\Framework\TestCase;

/**
 * Class HelloWorldTest
 * @package Hyperized\HelloWorld\Tests
 */
final class ConcatArrayTest extends TestCase
{
    /**
     * @var array
     */
    private static $input = [
        'aap',
        'noot',
        'mies',
    ];
    /**
     * @var string
     */
    private static $expectedOutput = "aap, noot, mies";
    /**
     * @var HelloWorld
     */
    private $helloWorld;

    /**
     * Provide us with a workable class instance
     */
    public function setUp(): void
    {
        $this->helloWorld = new HelloWorld();
    }

    /**
     * See if the output is as we expect
     */
    public function testOutput(): void
    {
        self::assertSame(self::$expectedOutput, $this->helloWorld::concat(self::$input));
    }
}
