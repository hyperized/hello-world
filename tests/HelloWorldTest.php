<?php declare(strict_types=1);

namespace Hyperized\HelloWorld\Tests;

use Hyperized\HelloWorld;
use PHPUnit\Framework\TestCase;

/**
 * Class HelloWorldTest
 * @package Hyperized\HelloWorld\Tests
 */
final class HelloWorldTest extends TestCase
{
    /**
     * @var string
     */
    private static $expectedOutput = 'Hello World!';
    /**
     * @var HelloWorld
     */
    private $helloWorld;

    /**
     * Provide us with a workable class instance
     */
    public function setUp(): void
    {
        $this->helloWorld = new HelloWorld();
    }

    /**
     * See if the output is as we expect
     */
    public function testOutput(): void
    {
        self::assertSame(static::$expectedOutput, $this->helloWorld::call_me_maybe());
    }
}
