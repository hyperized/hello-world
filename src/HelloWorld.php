<?php


namespace Hyperized;

class HelloWorld
{
    public static function call_me_maybe(): string
    {
        return 'Hello World!';
    }

    public static function concat($input)
    {
        $output = '';
        foreach ($input as $key => $value) {
            if ($key == 0) {
                $output = $value;
                continue;
            }
            $output .= ', ' . $value;
        }

        return $output;
    }
}
