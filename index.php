<?php

use Hyperized\HelloWorld;
use Klein\Klein;

require_once __DIR__ . '/vendor/autoload.php';

$klein = new Klein();

$klein->respond('GET', '/', static function () {
    return HelloWorld::call_me_maybe();

//    $array = [
//        'hello',
//        'world',
//    ];
//
//    return HelloWorld::concatArray($array);
//    return HelloWorld::concat($array);
});

$klein->dispatch();
